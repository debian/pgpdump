pgpdump (0.34-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set fields Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

  [ Daniel Kahn Gillmor ]
  * New upstream release
  * standards version: bump to 4.6.0 (no changes needed)
  * d/copyright: update dates
  * move to debhelper 13
  * drop patches, applied upstream
  * fix Vcs-Browser tag name
  * Rules-Requires-Root: no
  * d/watch: update to version 4

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 08 Jan 2022 16:15:34 -0500

pgpdump (0.33-2) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Fix testsuite, force UTC as timestamp
  * Team upload.

  [ Steve Langasek ]
  * Restore dropped debian/test.  Closes: #902828.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 12 Jul 2019 12:33:15 +0200

pgpdump (0.33-1) unstable; urgency=medium

  * release to unstable
  * Move to Debian GnuPG packaging team, coordinating with ghostbar
  * use debian/master branch

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 25 Jun 2018 17:57:29 -0400

pgpdump (0.33-0.2) experimental; urgency=medium

  * Non-maintainer upload to experimental
  * support DEB_BUILD_OPTIONS=nocheck (thanks, Helmut Grohne!)
  * fix cross-building (Closes: #894115)
  * avoid infinite loop (Closes: #869891)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 13 Jun 2018 13:28:38 -0400

pgpdump (0.33-0.1) experimental; urgency=medium

  * Non-maintainer upload to experimental
  * New upstream version
  * use DEP-14 branch naming
  * move Vcs-* to salsa
  * Standards-version: bump to 4.1.4 (no changes needed)
  * move to debhelper 11
  * d/changelog: drop trailing whitespace
  * d/copyright: add my name for debian packaging
  * convert to using upstream shell-based test suite

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 13 Jun 2018 10:58:07 -0400

pgpdump (0.32-0.1) experimental; urgency=medium

  * Non-maintainer upload to experimental

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 31 May 2017 09:16:17 -0400

pgpdump (0.31-0.2) unstable; urgency=medium

  * Non-maintainer upload
  * Fix failing test suite. Closes: #846678

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Wed, 14 Dec 2016 21:49:14 +0100

pgpdump (0.31-0.1) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * Non-maintainer upload (Closes: #845390, #773747)
  * use https URLs for Vcs-*
  * wrap-and-sort -ast
  * use dh_autoreconf
  * move to dh 10
  * imported patches from Peter Pentchev, already upstreamed
  * make debian/test work correctly
  * set up autopkgtest

  [ Peter Pentchev ]
  * Bump Standards-Version to 3.9.8 with no changes.
  * Switch to HTTPS for the copyright format spec URL, too.
  * Break the BSD-3-clause license into a separate section.
  * Drop the dirs file, the upstream build system creates them.
  * Enable all the hardening build options.
  * Switch to the 3.0 (quilt) source format.
  * Add Multi-Arch: foreign to the binary package.
  * Add an upstream metadata file.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 23 Nov 2016 01:23:35 -0500

pgpdump (0.29-1) unstable; urgency=medium

  * New upstream version

 -- Jose Luis Rivas <ghostbar@debian.org>  Wed, 09 Sep 2015 11:50:06 -0500

pgpdump (0.28-1) unstable; urgency=low

  * New upstream version.
  * debian/watch: mangle updated to match versioning on GitHub (with a v
    before the version number).
  * debian/copyright: updated to dep5.
  * debian/source/format: Created.
  * debian/control:
   + Standard-Versions to 3.9.4.
   + debhelper >= 9.
  * debian/compat: bump to 9.

 -- Jose Luis Rivas <ghostbar@debian.org>  Mon, 26 Aug 2013 10:45:17 -0430

pgpdump (0.27-1) unstable; urgency=low

  * New upstream release
  * Migrated to debhelper 7, removed cdbs and dependencies
  * Updated debian/watch, now pointing to github repo
  * Updated my email address on debian/control
  * Now dumps notation names (fixed upstream with patch from Riskó Gergely
  Closes: #464572)
  * Bumped to Standards-Version 3.9.1 wo/ changes.
  * Added Vcs-git and Homepage fields to debian/control.

 -- Jose Luis Rivas <ghostbar@debian.org>  Sat, 14 Aug 2010 22:23:30 -0430

pgpdump (0.26-1) unstable; urgency=low

  * New upstream version. (Closes: #443522)
  * debian/copyright:
   + Updated source URL.
  * debian/watch:
   + Updated watch URL.
  * debian/rules:
   + Migrated to CDBS, completely new `debian/rules`.
  * debian/control:
   + Standard-Versions updated to 3.7.3, no further modifications needed.
   + Added to Build-depends `cdbs`.

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Sun, 16 Dec 2007 02:13:45 -0430

pgpdump (0.25-1) unstable; urgency=low

  * New upstream version

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Thu,  7 Dec 2006 13:45:35 -0400

pgpdump (0.24-4) unstable; urgency=low

  * Fixed Long description on `debian/control`.

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Wed, 20 Sep 2006 16:35:39 -0400
pgpdump (0.24-3) unstable; urgency=low

  * New Mantainer. (closes: #383129)
  * Applied patch to `keys.c` given by Brian M. Carlson. (closes: #223837)

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Mon, 28 Aug 2006 23:35:07 -0400

pgpdump (0.24-2) unstable; urgency=low

  * QA upload.
  * Package is orphaned (#383129); set maintainer to Debian QA Group.
  * debian/watch: Update upstream URL.
  * Conforms to Standards version 3.7.2.

 -- Matej Vela <vela@debian.org>  Mon, 28 Aug 2006 08:28:52 +0200

pgpdump (0.24-1) unstable; urgency=low

  * New upstream release.

 -- Graham Wilson <graham@debian.org>  Fri, 24 Dec 2004 16:00:40 +0000

pgpdump (0.23-1) unstable; urgency=low

  * New upstream release.
  * Just call autoreconf in autogen.sh.
  * Update configure flags for newer autoconf versions.
  * Add a debian/test script to run run pgpdump with all of the files in the
    data subdirectory.

 -- Graham Wilson <graham@debian.org>  Tue, 23 Nov 2004 17:08:37 +0000

pgpdump (0.22-2) unstable; urgency=low

  * Correctly print labels for unencrypted v3 secret keys. (closes: #236553)

 -- Graham Wilson <graham@debian.org>  Thu, 11 Mar 2004 20:39:20 +0000

pgpdump (0.22-1) unstable; urgency=low

  * New upstream release.

 -- Graham Wilson <graham@debian.org>  Sun, 25 Jan 2004 04:49:55 +0000

pgpdump (0.21-1) unstable; urgency=low

  * Add a watch file for uscan.
  * New upstream version.
  * Add autogen.sh to regenerate auto* files.
  * Handle critical flag of subpacket type octet. (closes: #229020)

 -- Graham Wilson <graham@debian.org>  Fri, 23 Jan 2004 03:15:23 +0000

pgpdump (0.20-2) unstable; urgency=low

  * Do read from stdin, but handle EOF better. (closes: #223036)

 -- Graham Wilson <graham@debian.org>  Thu, 11 Dec 2003 04:04:11 +0000

pgpdump (0.20-1) unstable; urgency=low

  * New upstream version.
    - Support bzip2 compression from bis09 draft. (closes: #223035)
  * Correctly support DEB_BUILD_OPTIONS.
  * Don't read from stdin. (closes: #223036)

 -- Graham Wilson <graham@debian.org>  Wed, 10 Dec 2003 21:24:20 +0000

pgpdump (0.19-2) unstable; urgency=low

  * Change maintainer address.
  * Increment standards version. No changes.

 -- Graham Wilson <graham@debian.org>  Thu, 18 Sep 2003 18:32:54 +0000

pgpdump (0.19-1) unstable; urgency=low

  * Initial Debian release. (closes: #193093)

 -- Graham Wilson <bob@decoy.wox.org>  Fri, 08 Aug 2003 17:46:18 +0000

vim:ts=2:sw=2:et:
